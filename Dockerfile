FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
  clang            \
  cmake            \
  curl             \
  git              \
  libboost-all-dev \
  libpython3-dev   \
  python3          \
  python3-pip      \
  zlib1g-dev       \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain stable
ENV PATH $PATH:/root/.cargo/bin
RUN cargo install cargo-check
RUN rustup component add rust-src rustfmt-preview

WORKDIR /src
